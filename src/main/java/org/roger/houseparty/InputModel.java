package org.roger.houseparty;

public class InputModel {

    public Friend to;
    public Friend from;
    public Long timestamp;
    public Boolean areFriends;

    public Friend getTo() {
        return to;
    }

    public void setTo(Friend to) {
        this.to = to;
    }

    public Friend getFrom() {
        return from;
    }

    public void setFrom(Friend from) {
        this.from = from;
    }

    public Long getTs() {
        return timestamp;
    }

    public void setTs(Long ts) {
        this.timestamp = ts;
    }

    public Boolean getAreFriends() {
        return areFriends;
    }

    public void setAreFriends(Boolean areFriends) {
        this.areFriends = areFriends;
    }
}
