package org.roger.houseparty;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.LinkedHashMultimap;

import java.io.File;
import java.util.HashMap;
import java.util.Scanner;

public class JsonReader {
    private LinkedHashMultimap<String, Friend> friends = LinkedHashMultimap.create();
    private HashMap<String, Friend> people = new HashMap<>();

    private final String filename;
    private final Friendly friendly;

    JsonReader(String filename, Friendly friendly) {
        this.filename = filename;
        this.friendly = friendly;
    }

    LinkedHashMultimap<String, Friend> readInputFile() throws Exception {

        var scanner = new Scanner(new File(filename));
        var count = 0;
        var om = new ObjectMapper();

        System.out.print("Reading file ");
        while (scanner.hasNextLine()) {
            var line = scanner.nextLine();

            var o = om.readValue(line, InputModel.class);

            makeGraph(o);

            if (count++ % 10000 == 0)
                System.out.print(".");
        }

        System.out.println();
        return friends;
    }

    HashMap<String, Friend> getPeople() {
        return people;
    }

    private void makeGraph(InputModel action) {
        var to = action.to;
        var from = action.from;

        if (action.areFriends) {
            friends.put(to.getId(), from);
        } else {
            friends.remove(to.getId(), from);
        }

        people.put(to.getId(), to);
        people.put(from.getId(), from);
    }
}
