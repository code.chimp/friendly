package org.roger.houseparty;

public class Friend {

    public String id;
    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        var fr = (Friend) o;
        return ((Friend) o).getId().equals(this.id);
    }

    @Override
    public String toString() {
        return String.format("%s %s", id, name);
    }
 }
