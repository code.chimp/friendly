package org.roger.houseparty;

import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Lists;

import java.io.File;
import java.util.*;

public class Friendly {
    private LinkedHashMultimap<String, Friend> myFriends = null;
    private HashMap<String, Friend> people = null;

    private String filename;
    private String friendA;
    private String friendB;

    public static final void main(String[] args) throws Exception {
        var f = new Friendly(args);
        f.go();
    }

    public Friendly(String[] args) {
        if (args.length != 3) {
            usage(null);
            return;
        }

        filename = args[0];
        friendA = args[1];
        friendB = args[2];
    }

    private void usage(String msg) {
        if (msg == null) {
            System.out.println("malformed arguments");
        } else {
            System.out.println("error: " + msg);
        }
        System.out.println("Usage:\n\t java -jar <jarfile> <filename> <friend start> <friend end>");
    }

    private void go() throws Exception {
        var f = new File(filename);

        if (!f.exists()) {
            usage("file not found: " + filename);
            return;
        }

        var reader = new JsonReader(filename, this);

        myFriends = reader.readInputFile();
        people = reader.getPeople();

        if (!myFriends.containsKey(friendA) || !myFriends.containsKey(friendB)) {
            usage("unknown bacons");
            return;
        }
        var bacons = isBacon();
        System.out.println("bacon level: " + (bacons.isEmpty() ? "none" : bacons.size() - 1));
    }

    private List<Friend> isBacon() {
        var visited = new HashSet<>(List.of(friendA));
        var back = new HashMap<String, String>();
        var queue = new LinkedList<>(List.of(people.get(friendA)));

        Friend current;
        while ((current = queue.removeFirst()) != null) {
            if (current.getId().equals(friendB))
                break;

            for (Friend nextFriend : myFriends.get(current.getId())) {
                if (!visited.contains(nextFriend.getId())) {
                    queue.addLast(nextFriend);
                    visited.add(nextFriend.getId());
                    back.put(nextFriend.getId(), current.getId());
                }
            }
        }

        var bacons = new LinkedList<Friend>();
        if (!current.getId().equals(friendB))
            return bacons;

        for (String friend = friendB; friend != null; friend = back.get(friend)) {
            bacons.add(people.get(friend));
        }

        return Lists.reverse(bacons);
    }
}
