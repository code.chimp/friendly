
# design issues
My first decision was what do with the input json.   Deserialize it  or string parse
the fields and create the friends myself.    I opted for fasterxml as it
was faster than parsing the text directly to implement.

The second decision was whether to store friend objects or friend or ids.  It ended
that I key on friendId on the object to store the Friend objects.   Ie I always
know my friends info, but I have to look my info up.

I opted to give feedback on reading in the file as its not instantaneous and
its a a CLI.   The expected behavior is 'don't let me wait in the dark', I went with '.'
for every 10k records processed.

Doing the lookups in the friend map turned out to be zippy and not requiring any special
status.

Bacons is a measure of the space between friends.   Two friends have a bacon
value of 1, three sequential friends 2.

Last the timestamp in the input wasn't used anywhere?

# how to build
## prerequisites

* java 10
* gradle, installed from https://gradle.org/install/
* access to the web so maven dependencies can be downloaded

## building
* unizip the zip file into a directory, say 'friendly'
* after getting your java build env up to snuff, an exercise left to the reader type

$ cd friendly

$ gradle build shadowJar

## running


$ gradle build shadowJar\
$ java -jar build/libs/friends-1.0-SNAPSHOT-all.jar <path to friends.txt> <user1> <user2>\

You should see something like:\
$ java -jar build/libs/friends-1.0-SNAPSHOT-all.jar /Users/rogerwarner/Downloads/friends.txt ocSid/zVZy4= vUMVsYootTE=\
Reading file ...............................................................\
bacon level: 11
